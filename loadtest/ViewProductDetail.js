var casper = require('casper').create({
			pageSettings: {
				loadImages: false,
				loadPlugins: false
			}
		}),
	utils = require("utils"),
	config = require("config");

var productId = casper.cli.get(0),
	loadingTime = 0, 
	loginTime = 0,
	start = new Date().getTime(),
	homeUrl = config.domain + '/product-slug-go-here-p'+productId+'-f1-2';

phantom.clearCookies();
casper.start(homeUrl);
casper.clear();
casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36');

casper.run(function() {
	var data = [
        {
        	type: "detail",
        	time: new Date().getTime() - start
        }
    ];
	this.echo(JSON.stringify(data));
	this.exit();
});