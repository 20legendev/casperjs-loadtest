var casper = require('casper').create({
		pageSettings: {
			loadImages: false,
			loadPlugins: false,
			
		}
	}),
	utils = require("utils"),
	config = require("config");

var productId = casper.cli.get(0),
	loadingTime = 0, 
	cartTime = 0,
	home = config.domain + '/product-mat-na-nghe-p'+productId+'-f1-0',
	start = new Date().getTime(),
	orderLoaded = 0,
	bynow = 0;

phantom.clearCookies();
casper.start(home);
casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36');
casper.clear();

casper.options.onResourceRequested = function(C, requestData, request) {
  if ((/https?:\/\/.+?\.css/gi).test(requestData['url']) || requestData['Content-Type'] == 'text/css') {
    request.abort();
  }
};


casper.on('resource.received', function(r){
	if(r.url.indexOf('order.adayroi') != -1 && orderLoaded == 0 && r.stage == 'end'){
		orderLoaded = 1;
		this.emit('order.loaded');
	}
	if(r.url == 'https://'+ config.blank_domain +'/Home/Submit/'){
		this.emit('order.submitted');
	}
});

casper.then(function() {
	this.waitForSelector('.btn-purchase', function() {
		loadingTime = new Date().getTime() - start;
		this.click('.btn-purchase');
		start = new Date().getTime();
	}, function(){
		cartTime = 10000;
	}, 10000);
});
casper.on('order.loaded', function(){
	bynow = new Date().getTime() - start;
	var url = this.getCurrentUrl().split('/');
	var data = {
	    	id: url[url.length - 1],
			model: {
				BillingAddress: "",
				BuyerAddress: {
					Email: "systemadmin@adayroi.vn",
					Id: 717
				},
				Email: "systemadmin@adayroi.vn",
				InvoiceName: "",
				InvoiceTaxCode: "",
				IsCreditUse: false,
				IsNewBuyerAddress: true,
				PaymentGatewayId: 29,
				ShippingAddress: {
					Email: "systemadmin@adayroi.vn",
					Id: 717
				},
				ShippingTimeByProduct: [
					{
						key: 29198, 
						value: {
							DateTimeView: "17/02/2015 20:00"
						}
					}
				],
				TakeVAT: false
			}
		};
	start = new Date().getTime();
	casper.open('https://' + config.blank_domain + '/Home/Submit/', {
	    method: 'POST',
	    data: data
	}, function response(data){
		console.log(utils.dump(data));
	});
});

casper.on('order.submitted', function(){
	submitTime = new Date().getTime() - start;
	var data = 
		[{
			type: 'order',
			time: bynow
		},{
			type: 'order_submit',
			time: submitTime
		}];
	this.echo(JSON.stringify(data));
	this.exit();
});

casper.run(function(){});