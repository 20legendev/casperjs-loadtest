var casper = require('casper').create({
	pageSettings: {
		loadImages: true,
		loadPlugins: false
	}
});
var	utils = require("utils"),
	config = require("config");

var categoryId = casper.cli.get(0);
var homeUrl = config.domain + '/kinh-mat/c-' + categoryId;
var start = new Date().getTime();
var data = [];

casper.start(homeUrl);
casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36');
casper.viewport(1280, 800);
casper.then(function() {
	data.push({
		type: 'category',
		time: new Date().getTime() - start
	});
	var name = this.evaluate(function(){
		return name;
	});
	homeUrl = config.domain + "/" + name + '/c-' + categoryId;
	casper.capture("../screenshot/1.png");
	this.click('#order_0 a');
	start = new Date().getTime();
});
casper.then(function() {
	this.waitForUrl(homeUrl + '/0/1/', function(){
		data.push({
			type: 'order',
			time: new Date().getTime() - start
		});
		this.click('#order_1 a');
		start = new Date().getTime();
	});
});
casper.then(function() {
	this.waitForUrl(homeUrl + '/1/1/', function(){
		data.push({
			type: 'order',
			time: new Date().getTime() - start
		});
		this.click('#order_2 a');
		start = new Date().getTime();
	});
});
casper.then(function() {
	this.waitForUrl(homeUrl + '/2/1/', function(){
		data.push({
			type: 'order',
			time: new Date().getTime() - start
		});
		this.click('#order_3 a');
		start = new Date().getTime();
	});
});
casper.then(function() {
	this.waitForUrl(homeUrl + '/3/1/', function(){
		data.push({
			type: 'order',
			time: new Date().getTime() - start
		});
		this.click('#order_4 a');
		start = new Date().getTime();
	});
});
casper.then(function() {
	this.waitForUrl(homeUrl + '/4/1/', function(){
		data.push({
			type: 'order',
			time: new Date().getTime() - start
		});
	});
});

casper.run(function() {
	var end = new Date().getTime();
	this.echo(JSON.stringify(data));
	this.exit();
});