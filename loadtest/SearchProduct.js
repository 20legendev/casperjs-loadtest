var casper = require('casper').create({
	pageSettings: {
		loadImages: false,
		loadPlugins: false
	}
});
var utils = require("utils"),
	config = require("config");
	
var	searchKey = casper.cli.get(0),
	start = 0,
	end = 0;


casper.start(config.domain + "/");
casper.clear();
casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36');

casper.then(function(){
	casper.waitForSelector('#inputSearch', function(){
		this.sendKeys('#inputSearch', searchKey);
		start = new Date().getTime();
		this.click('input[type="button"][value="Tìm ngay"]');
	}, function(){
		start = new Date().getTime();
	}, 20000);
});
casper.then(function(){
	this.waitForUrl(/search*/, function(){
		end = new Date().getTime();
	}, function(){
		end = new Date().getTime();
	}, 20000);
});
casper.run(function() {
	var data = [
        {
        	type: "search",
        	time: end - start
        }
    ];
	this.echo(JSON.stringify(data));
	this.exit();
});
