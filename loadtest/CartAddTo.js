var casper = require('casper').create({
		pageSettings: {
			loadImages: false,
			loadPlugins: false
		}
	}),
	utils = require("utils"),
	config = require("config");

var productId = casper.cli.get(0),
	loadingTime = 0, 
	cartTime = 0,
	home = config.domain + '/product-mat-na-nghe-p'+productId+'-f1-0',
	start = new Date().getTime();

phantom.clearCookies();
casper.start(home);
casper.clear();
casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36');

casper.then(function() {
	this.capture('../screenshot/b.png');
	casper.waitForSelector('.btn-add-to-card', function() {
		loadingTime = new Date().getTime() - start;
		this.evaluate(function(){
			buyProduct(1);
		});
		start = new Date().getTime();
		this.waitForResource(config.domain + '/cart/addproducttocart', function(data){
			end = new Date().getTime();
			cartTime = end - start;
		}, function(){
			cartTime = 10000;
		}, 10000);
	}, function(){
		cartTime = 10000;
	}, 10000);
});
casper.run(function() {
	var data = [
        {
        	type: 'detail',
        	time: loadingTime
        },
        {
        	type: 'addtocart',
        	time: cartTime
        }
    ];
	this.echo(JSON.stringify(data));
	this.exit();
});