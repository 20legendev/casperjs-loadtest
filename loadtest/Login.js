var casper = require('casper').create({
	pageSettings: {
		loadImages: false,
		loadPlugins: false
	}
});
var utils = require("utils"),
	config = require("config");

var email = casper.cli.get(0),
	password = casper.cli.get(1),
	loadingTime = 0, 
	loginTime = 0,
	start = new Date().getTime();

phantom.clearCookies();

casper.start(config.domain + "/");
casper.clear();
casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36');

casper.then(function() {
	loadingTime = new Date().getTime() - start;
	this.waitForSelector('#ulNotLogin li:nth-child(2) a', function() {
		this.click('#ulNotLogin li:nth-child(2) a');
	});
});
casper.then(function(){
	this.withFrame(1, function(){
		this.waitForSelector('form', function(){
			start = new Date().getTime();
			this.fill('form', {
				Email: email, 
				Password: ""+password
			}, true);
		});
	});
	this.waitForResource(config.domain + '/login/doLogin', function(data){
		loginTime = new Date().getTime() - start;
	});
});
casper.run(function() {
	var data = [
        {
        	type: "login",
        	time: loginTime
        },
        {
        	type: "home",
        	time: loadingTime
        }
    ];
	this.echo(JSON.stringify(data));
	this.exit();
});