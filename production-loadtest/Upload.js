var fs = require('fs'),
	casper = require('casper').create({
		pageSettings: {
			loadImages: false,
			loadPlugins: false
		}
	}),
	utils = require("utils");

var cookie = fs.read('cookie.txt'),
	loadingTime = 99999, 
	loginTime = 99999,
	start = new Date().getTime(),
	ajaxDoneTime = 99999;
	
	phantom.cookies = JSON.parse(cookie);


casper.on('resource.received', function(r){
	// console.log(utils.dump(r));
	console.log(r.url);
});

casper.start('http://vinic.adayroi.ld/vinic/ext/#sx.chupanhsanpham/50795');
casper.viewport(1280, 800);

casper.then(function() {
	/*
	this.waitForSelector('span.x-btn-inner.x-btn-inner-default-toolbar-small', function(){
		this.click('span.x-btn-inner.x-btn-inner-default-toolbar-small:contains("Tải lên")');
	}, function(){
		this.capture('../screenshot/8.png');
		console.log("TO");
	}, 20000);
	*/
	casper.waitFor(function check() {
        return this.evaluate(function() {
            return $('span.x-btn-inner.x-btn-inner-default-toolbar-small:contains("Tải lên")').length > 0;
        });
    }, function then() {
		this.evaluate(function(){
			$('span.x-btn-inner.x-btn-inner-default-toolbar-small:contains("Tải lên")').trigger('click');
		});    	
    }, function timeout() { // step to execute if check has failed
    	console.log("TO");
    },10000);
});

casper.then(function(){
	casper.waitFor(function check() {
        return this.evaluate(function() {
            return $('span.x-btn-inner.x-btn-inner-default-toolbar-small:contains("Tải lên")').length == 2;
        });
    }, function then() {
		this.evaluate(function(){
			$('.x-form-file-input').val('/home/kiennt/Pictures/Orange-bobber.jpg');
		});
		casper.then(function(){
			console.log(this.evaluate(function(){
				return $('.x-form-file-input').val();
			}));
		});    	
    }, function timeout() { // step to execute if check has failed
    	console.log("TO");
    },10000);
});


casper.run(function() {
	var data = [
		/*
        {
        	type: 'homeafterlogin',
        	time: loadingTime
        },
        
        {
        	type: 'category',
        	time: ajaxDoneTime 
        }
        */
    ];
	this.echo(JSON.stringify(data));
	this.exit();
});