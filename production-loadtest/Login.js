var fs = require('fs');

var casper = require('casper').create({
	pageSettings: {
		loadImages: true,
		loadPlugins: false
	}
});
var utils = require("utils");
var loadingTime = 99999, loginTime = 99999;

var start = new Date().getTime();
var home = 'http://vinic.adayroi.ld/vinic/ext/';

casper.start(home);
casper.viewport(1280, 800);

casper.then(function() {
	this.waitForSelector('input[type="password"]', function(){
		loadingTime = new Date().getTime() - start;
		this.sendKeys('input[type="text"]', 'vinecom');
		this.sendKeys('input[type="password"]', '1');
		this.click('span.x-btn-inner-default-small');
		start = new Date().getTime();
	}, function(){}, 10000);
	this.waitForSelector('div.x-box-menu-body-horizontal', function(){
		loginTime = new Date().getTime() - start;
		var cookies = JSON.stringify(phantom.cookies);
		fs.write('cookie.txt', cookies, 644);
	}, function(){}, 20000);
});


casper.run(function() {
	var data = [
        {
        	type: 'homepage',
        	time: loadingTime
        },
        {
        	type: 'login',
        	time: loginTime
        }
    ];
	this.echo(JSON.stringify(data));
	this.exit();
});