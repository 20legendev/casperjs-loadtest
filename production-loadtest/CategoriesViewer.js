var fs = require('fs'),
	casper = require('casper').create({
		pageSettings: {
			loadImages: true,
			loadPlugins: true
		}
	}),
	utils = require("utils");

var cookie = fs.read('cookie.txt'),
	loadingTime = 99999, 
	loginTime = 99999,
	start = new Date().getTime(),
	ajaxDoneTime = 99999;
	
	phantom.cookies = JSON.parse(cookie);

var ran = casper.cli.get(0);

var home = 'http://vinic.adayroi.ld/vinic/ext/',
	dic = 'http://vinic.adayroi.ld/vinic/',
	data = [
		{
			url: '#kh.kehoachsps',
			ajax: 'web/center.svc/modelmanager/KH.YcSanPham.Khsp'
		}, {
			url: '#kh.kehoachdvs',
			ajax: 'web/center.svc/modelmanager/KH.SkuDichVu.Khdv'
		}, {
			url: '#kh.kehoachthangs',
			ajax: 'web/center.svc/modelmanager/KH.YeuCau.KhThang'
		}, {
			url: '#kh.kehoachtuans',
			ajax: 'web/center.svc/modelmanager/KH.YeuCau.KhTuan'
		}, {
			url: '#kh.yeucaukhaosats',
			ajax: 'web/center.svc/modelmanager/KH.YeuCau.KhaoSat'
		}, {
			url: '#kh.khlaymaus',
			ajax: 'web/center.svc/modelmanager/KH.KhLayMau.YeuCau'
		}, {
			url: '#kh.ycdichvunhanves',
			ajax: 'web/center.svc/modelmanager/KH.YcDichVu.NhanVe'
		}, {
			url: '#kh.kehoachngaylists',
			ajax: 'web/center.svc/modelmanager/KH.YeuCau.KhNgay'
		}, {
			url: '#kh.ycsanphamnhanves',
			ajax: 'web/center.svc/modelmanager/KH.YcSanPham.NhanVe'
		}, {
			url: '#km.bins',
			ajax: 'web/center.svc/modelmanager/KM.Bin.List'
		}, {
			url: '#km.binsanphams',
			ajax: 'web/center.svc/modelmanager/KM.GiaoDichNhap.BinList'
		}, {
			url: '#km.khos',
			ajax: 'web/center.svc/modelmanager/KM.Kho.List'
		}, {
			url: '#km.khoxuats',
			ajax: 'web/center.svc/modelmanager/KM.GiaoDichXuat.List'
		}, {
			url: '#sx.ctanhdichvus',
			ajax: 'web/center.svc/modelmanager/SX.CtAnhDichVu.List'
		}, {
			url: '#sx.ctanhsanphams',
			ajax: 'web/center.svc/modelmanager/SX.CtAnhSanPham.List'
		}, {
			url: '#sx.chupanhdichvus',
			ajax: 'web/center.svc/modelmanager/SX.ChupAnhDichVu.List'
		}, {
			url: '#sx.chupanhsanphams',
			ajax: 'web/center.svc/modelmanager/SX.ChupAnhSanPham.List'
		}, {
			url: '#sx.sxdichvus',
			ajax: 'web/center.svc/modelmanager/SX.SxDichVu.List'
		}, {
			url: '#sx.kddichvus',
			ajax: 'web/center.svc/modelmanager/SX.KdDichVu.List'
		}, {
			url: '#sx.kdsanphams',
			ajax: 'web/center.svc/modelmanager/SX.KdSanPham.List'
		}, {
			url: '#sx.sxsanphams',
			ajax: 'web/center.svc/modelmanager/KH.YcSanPham.List'
		}, {
			url: '#sx.vietnddichvus',
			ajax: 'web/center.svc/modelmanager/SX.VietNdDichVu.List'
		}, 
	];
var test = data[ran];
test.dic = dic + test.ajax;
test.ajax = home + test.ajax;

casper.start(home);
casper.viewport(1280, 800);

casper.then(function() {
	this.waitForSelector('div.x-box-inner.x-box-menu-body-horizontal', function(){
		loadingTime = new Date().getTime() - start;
		start = new Date().getTime();
	}, function(){
		console.log("TO");
	}, 20000);
});

casper.then(function(){
	this.evaluate(function(url){
		top.location.hash = url;
	}, test.url);
	this.waitForResource(test.dic, function(){
		ajaxDoneTime = new Date().getTime() - start;
	});
});

casper.run(function() {
	var data = [
		/*
        {
        	type: 'homeafterlogin',
        	time: loadingTime
        },
        */
        {
        	type: 'category',
        	time: ajaxDoneTime 
        }
    ];
	this.echo(JSON.stringify(data));
	this.exit();
});