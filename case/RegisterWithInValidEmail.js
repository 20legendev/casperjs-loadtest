// casper.test.begin('ADR.Search.Popup', 2, {

function checkProductRendered(test) {
	test.assertExists('.headline');
}

function checkPopup(){
	casper.test.assertVisible('.loginbox');
	casper.test.assertExists('.loginbox iframe');
}

function checkIframeLoaded(){
}

function insertPrediction(casper){
    this.fill('form#contact-form', {
        'subject':    'I am watching you',
        'content':    'So be careful.',
        'civility':   'Mr',
        'name':       'Chuck Norris',
        'email':      'chuck@norris.com',
        'cc':         true,
        'attachment': '/Users/chuck/roundhousekick.doc'
    }, true);
}

casper.test.begin('RegisterWithInValidEmail', {
    setUp: function(test) {
    },

    tearDown: function(test) {
    },

    test: function(test) {
    	var dataSearch = 'Kẹp';
    	casper.start('http://adayroi.vn');
    	casper.then(function(){
    		test.assertHttpStatus(200);
    		test.assertExists('a[title="Đăng ký"]', "Register button exists");
    		// this.sendKeys('#inputSearch', dataSearch);
    		this.click('a[title="Đăng ký"]');
		});
		casper.then(function(){
			casper.wait(1000, function(){
				checkPopup();
			});
		});
		casper.then(function(){
			this.echo(this.getCurrentUrl());
			this.wait(1000, function(){
				this.echo(this.getCurrentUrl());
				this.withFrame(1, function(){
					// this.echo(this.getCurrentUrl());
				    this.capture('../screenshot/' + new Date().getTime() + '.png', {
				        top: 0,
				        left: 0,
				        width: 1000,
				        height: 1000
				    });

				});
			});
		});
		
    	casper.run(function() {
	        test.done();
	    });
    }
});
