var casper = require('casper').create();
var links = [];

function getLinks() {
    var links = document.querySelectorAll('h3.r a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
}
casper.start('http://google.com.vn/', function() {
    // search for 'casperjs' from google form
    this.fill('form[action="/search"]', { q: 'kiennguyen07' }, true);
});

casper.then(function() {
    links = this.evaluate(getLinks);
    this.fill('form[action="/search"]', { q: 'letrang0407' }, true);
});

casper.then(function() {
    // aggregate results for the 'phantomjs' search
    links = links.concat(this.evaluate(getLinks));
});

casper.run(function() {
    this.echo(links.length + ' links found:');
    this.echo(' - ' + links.join('\n - ')).exit();
});

