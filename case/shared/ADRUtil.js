var require = patchRequire(require);
ADRUtil = {

	capture: function(casper){
		casper.capture('../screenshot/' + new Date().getTime() + '.png');
	},
	
	getNumber: function(string){
		return string.replace( /^\D+/g, '');
	}
};

module.exports = ADRUtil;