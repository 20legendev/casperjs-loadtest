var require = patchRequire(require);
var ADRUtil = require('./ADRUtil');

ADRLib = {

	login: function(email, password, casper, test){
		casper.click('.sign-in li:first-child a');
		casper.wait(2000, function(){
    		casper.withFrame(1, function(){
			    this.fill('form', {
			        'Email':    email,
			        'Password':    password
			    }, true);
			    casper.then(function(){
			    	casper.wait(5000, function(){
			    		var loginSuccess = this.evaluate(function(email){
			    			var sp = $('span[id="support-customer"]');
			    			if(sp.length == 2){
			    				var html = $(sp[1]).html();
			    				if(html.indexOf(email) >= 0) return true;
			    				return false;
			    			}
			    			return false;
			    		}, email);
			    		ADRUtil.capture(casper);
			    		test.assertEquals(true, loginSuccess);
			    		
			    	});
			    });
    		});
		});
	},
	
	search: function(keyword, casper, test){
		test.assertExists('#inputSearch', "Input search found");
		casper.sendKeys('#inputSearch', keyword);
		casper.click('input[type="button"][value="Tìm ngay"]');
		
		casper.then(function(){
			casper.wait(1000, function(){
				test.assertNotVisible('.modal-custom');
				test.assertTitle("Từ khóa '" + keyword + "' - Adayroi", this.getTitle());
				test.assertExists('.headline');
			});
		});
	}
};
module.exports = ADRLib;