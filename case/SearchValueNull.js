// casper.test.begin('ADR.Search.Popup', 2, {
casper.test.begin('ADR.Search.Popup', {
    setUp: function(test) {
    },

    tearDown: function(test) {
    },

    test: function(test) {
    	casper.start('http://adayroi.vn');
    	casper.then(function(){
    		test.assertHttpStatus(200);
    		test.assertExists('#inputSearch', "Input search found");
    		this.click('input[type="button"][value="Tìm ngay"]');
		});
		casper.wait(1000, function(){
			casper.then(function(){
				test.assertVisible('.modal-custom');
			});
		});
		
    	casper.run(function() {
	        test.done();
	    });
    }
});
