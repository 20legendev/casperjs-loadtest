// casper.test.begin('ADR.Search.Popup', 2, {

function checkProductRendered(test) {
	test.assertExists('.headline');
}

casper.test.begin('ADR.Search.Popup', {
    setUp: function(test) {
    },

    tearDown: function(test) {
    },

    test: function(test) {
    	var dataSearch = 'kem đậu xanh';
    	casper.start('http://adayroi.vn');
    	casper.then(function(){
    		test.assertHttpStatus(200);
    		test.assertExists('#inputSearch', "Input search found");
    		this.sendKeys('#inputSearch', dataSearch);
    		this.click('input[type="button"][value="Tìm ngay"]');
		});
		casper.then(function(){
			casper.wait(1000, function(){
				test.assertNotVisible('.modal-custom');
				test.assertTitle("Từ khóa '" + dataSearch + "' - Adayroi", this.getTitle());
				checkProductRendered(test); 
			});
		});
		
    	casper.run(function() {
	        test.done();
	    });
    }
});
