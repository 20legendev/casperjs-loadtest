var ADRLib = require('./shared/ADRLib');
var ADRUtil = require('./shared/ADRUtil');

casper.test.begin('CheckoutbybasketSimpleProductbyVisaCard', {
    setUp: function(test) {
    	
    },

    tearDown: function(test) {
    },

    test: function(test) {
    	casper.start('http://adayroi.vn/');
    	casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36');
    	casper.viewport(1280, 800);
    	// Login
    	
    	casper.then(function(){
    		ADRLib.login('adminauthen@adayroi.com', '123456', casper, test);
		});
		// Search with search query 1
		casper.then(function(){
			ADRLib.search('keo', casper, test);
		});
		// Get price for first product on search page and move to detail page
		var sPrice = 0;
		var dPrice = 1;
		casper.then(function(){
			test.assertExists('.wrap-list-item .col-xs-3');
			sPrice = this.evaluate(function(){
				var item = $('.wrap-list-item .col-xs-3').first();
				return item.find('.item-price span').html();
			});
			casper.click('.wrap-list-item .col-xs-3:first a:first');
		});
		// Check price on detail page
		casper.then(function(){
			dPrice = this.evaluate(function(){
				var price = $('.wrap-product-detail .item-price .amount-1').html();
				return ADRUtil.getNumber(price);
			});
			this.echo(dPrice);
		});
		
		casper.then(function(){
			casper.echo(sPrice + ' ' + dPrice);
			// test.assertEqual(sPrice, dPrice);
		});
    	/*
    	ADRUtil.capture();
    	*/
    	/*
    	casper.then(function(){
    		
		});
		*/
    	casper.run(function() {
	        test.done();
	        phantom.clearCookies();
	    });
    }
});
